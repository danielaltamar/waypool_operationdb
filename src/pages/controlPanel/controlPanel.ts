import { Component } from '@angular/core';
import { NavController, ViewController, IonicPage, NavParams, AlertController } from 'ionic-angular';
import { operationDBService } from '../../services/operationDB.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Subject } from 'rxjs';
import * as moment from 'moment';


@IonicPage()
@Component({
  selector: 'page-controlpanel',
  templateUrl: 'controlpanel.html'
})
export class ControlPanelPage {
 allUsers:any =[];
 drivers:any =[];
 allTrips:any =[];
 timesUserGoListride:any =[];
 allReserves:any =[];

 driver:any;
 

 universityName:any;
 uid:any;
 showDriver:boolean;
  constructor(public navCtrl: NavController, public afDB: AngularFireDatabase, public alertCtrl:AlertController,public AngularFireAuth: AngularFireAuth,public viewCtrl: ViewController,public navParams: NavParams ,public operationDBService:operationDBService) {    
   this.universityName = this.navParams.get('university');
   moment.locale('es'); //to make the date be in spanish  
   console.log(this.universityName)
     


 
  
}

 
  verifyDocuments(){
    
    this.operationDBService.getDriver(this.universityName,this.uid);
    const alert = this.alertCtrl.create({
      title: 'DRIVERE',
      subTitle: 'DRIVER',
      buttons: ['DRIVER']
    });
    alert.present();
  }
  markTrueLicense(){
    
    this.operationDBService.markLicenseTrue(this.universityName,this.uid);
    if(this.uid === undefined || this.uid === null){
      const alert = this.alertCtrl.create({
        title:'error' ,
        subTitle: `coloca un uid`,
        buttons: ['ok']
      });
      alert.present();
    }else{
      const alert = this.alertCtrl.create({
        title:'exito' ,
        subTitle: `El driver con uid ${this.uid} y de universidad ${this.universityName} tiene el licencia = true`,
        buttons: ['ok']
      });
      alert.present();
    }
  }
  markTrueId(){
    console.log(this.uid)
    this.operationDBService.markIdTrue(this.universityName,this.uid);
    if(this.uid === undefined || this.uid === null){
      const alert = this.alertCtrl.create({
        title:'error' ,
        subTitle: `coloca un uid`,
        buttons: ['ok']
      });
      alert.present();
    }else{
      const alert = this.alertCtrl.create({
        title:'exito' ,
        subTitle: `El driver con uid ${this.uid} y de universidad ${this.universityName} tiene el id = true`,
        buttons: ['ok']
      });
      alert.present();
    }
  }
  markTrueCarne(){
    console.log(this.uid)
    this.operationDBService.markCarneTrue(this.universityName,this.uid);
    if(this.uid === undefined || this.uid === null){
      const alert = this.alertCtrl.create({
        title:'error' ,
        subTitle: `coloca un uid`,
        buttons: ['ok']
      });
      alert.present();
    }else{
      const alert = this.alertCtrl.create({
        title:'exito' ,
        subTitle: `El driver con uid ${this.uid} y de universidad ${this.universityName} tiene el carne = true`,
        buttons: ['ok']
      });
      alert.present();
    }
 
  }
 confirmDriver(){
  this.operationDBService.getDriver(this.universityName,this.uid).subscribe(driver=>{
    this.driver=driver
    console.log(this.driver)
    console.log(this.uid)
    if(this.driver === undefined || this.driver === null){
      this.showDriver = false;
      const alert = this.alertCtrl.create({
        title:'exito' ,
        subTitle: `El driver con uid ${this.uid} no se encuentra en el path, revisa el uid o la DB`,
        buttons: ['ok']
      });
      alert.present();
    }else{
      this.showDriver = true;
    }
  })


}  
  dismiss() {
    }  
}
