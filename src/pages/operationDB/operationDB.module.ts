
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { operationDBPage } from './operationDB';
 
@NgModule({
  declarations: [
    operationDBPage,
  ],
  imports: [
    IonicPageModule.forChild(operationDBPage),
  ],
  exports: [
    operationDBPage
  ]
})
export class operationDBPageModule {}