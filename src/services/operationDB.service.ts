import { AngularFireDatabase } from "@angular/fire/database";
import { Injectable } from "@angular/core";

import { AngularFireAuth } from "angularfire2/auth";

@Injectable()
export class operationDBService {
    constructor(public afDB: AngularFireDatabase, public AngularFireAuth: AngularFireAuth){
      
    }

   
  
  public getTheReserves(university){
     
    return  this.afDB.list('/data/allReservesCreated/'+university).valueChanges();
} 
public getUniversity(){
     
  return  this.afDB.list('universities').valueChanges();
} 
public getTimesUserGoListride(university){
     
  return  this.afDB.list('/data/timesUserGoListride/'+university).valueChanges();
} 

public getTripsInitiated(university){
     
  return  this.afDB.list('/data/allTripsInitiated/'+university).valueChanges();
} 
  public getTheUsers(university){
     
    return  this.afDB.list(university+'/users/').valueChanges();
} 
public getTheDrivers(university){
     
  return  this.afDB.list(university+'/drivers/').valueChanges();
} 
public getSpecificUser(userId){
     
  return  this.afDB.object('/users/'+ userId).valueChanges();
} 
public getSpecificDriver(userId){
     
  return  this.afDB.object('/drivers/'+ userId).valueChanges();
} 
    public insertUser(userId,user){
      // insert all users  
    this.afDB.database.ref('/users/'+ userId).update(user);

   }   
   public markLicenseTrue(university,userId){
    // insert all users  
  this.afDB.database.ref(university+'/drivers/'+ userId+'/documents/').update({
    license:true
  });

 } 
 public markIdTrue(university,userId){
  // insert all users  
this.afDB.database.ref(university+'/drivers/'+ userId+'/documents/').update({
  id:true
});
this.afDB.database.ref(university+'/users/'+ userId+'/documents/').update({
  id:true
});

} 
public markCarneTrue(university,userId){
  // insert all users  
this.afDB.database.ref(university+'/drivers/'+ userId+'/documents/').update({
  carne:true
});
this.afDB.database.ref(university+'/users/'+ userId+'/documents/').update({
  carne:true
});
} 
   public pushPhoneDrivers(phone){
    // insert all users  
  this.afDB.database.ref('/driver_phones/').push(phone);

 }   
 public pushPhoneUsers(phone){
  // insert all users  
this.afDB.database.ref('/users_phones/').push(phone);

}  
public sendData(date,trips,reserves,viewsFromUser){
  // insert all users  
this.afDB.database.ref('/RECORDS_DE_CRECIMIENTO/'+date).update({
  trips:trips,
  reserves:reserves,
  viewFromUser:viewsFromUser
});
}
public getDriver(university,userId){
  // insert all users  
  return  this.afDB.object(university+'/drivers/'+ userId).valueChanges();


}  
   public removePasswords(userId){
    // insert all users  
  this.afDB.database.ref('/drivers/'+ userId +'/password').remove();
  this.afDB.database.ref('/drivers/'+ userId +'/passwordconf').remove();

 }    

     }

     



    

