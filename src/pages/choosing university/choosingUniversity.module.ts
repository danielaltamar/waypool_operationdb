
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { choosingUniversityPage } from './choosingUniversity';
 
@NgModule({
  declarations: [
    choosingUniversityPage,
  ],
  imports: [
    IonicPageModule.forChild(choosingUniversityPage),
  ],
  exports: [
    choosingUniversityPage
  ]
})
export class choosingUniversityPageModule {}