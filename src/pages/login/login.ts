import { Component } from '@angular/core';

import { NavController, AlertController, NavParams, IonicPage, Platform } from 'ionic-angular';


import { AngularFireAuth } from 'angularfire2/auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http } from '@angular/http';
// import * as firebase from 'firebase';
// import { SignUpService } from '../../services/signup.service';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

    email:string = '';
    password:string;
    auth = this.AngularFireAuth.auth;
    receivedUser;
    private loginGroup: FormGroup;
    driverInfo:any;
    // userFirebase = this.AngularFireAuth.auth.currentUser;
    
  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private AngularFireAuth: AngularFireAuth, public navParams: NavParams, private formBuilder: FormBuilder,  public platform: Platform) {
    this.loginGroup = this.formBuilder.group({
        email: ["", Validators.required],
        password: ["", Validators.required]
    })
      
}

    
    logIn(){      

        let email = this.loginGroup.controls['email'].value;
        let password = this.loginGroup.controls['password'].value;

        this.AngularFireAuth.auth.signInWithEmailAndPassword(email, password).then((data) => {
                console.log(data)
                        this.navCtrl.push('choosingUniversityPage');
                        })
                    .catch((error) => {
                const alert = this.alertCtrl.create({
                    title: 'Oops!',
                    subTitle: 'El usuario o la contraseña están incorrectas',
                    buttons: ['OK']
                  });
                  alert.present();
                console.log(error);
            });
        }
}