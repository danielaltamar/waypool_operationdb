import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleMaps } from '@ionic-native/google-maps';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { Firebase } from '@ionic-native/firebase';
import { Geolocation } from '@ionic-native/geolocation';

import { CallNumber } from '@ionic-native/call-number';
import { CommonModule } from '@angular/common';

import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
  
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { operationDBService } from '../services/operationDB.service';




export const firebaseConfig = {
  
    apiKey: "AIzaSyB7Py2pOZEUJD2Ar34a-8z-rReiDtsikxw",
    authDomain: "waypool-511be.firebaseapp.com",
    databaseURL: "https://waypool-511be.firebaseio.com",
    projectId: "waypool-511be",
    storageBucket: "waypool-511be.appspot.com",
    messagingSenderId: "904521954579",
    appId: "1:904521954579:web:ec5463ca11c208b4"
};
// waypoolSecurityRules
// apiKey: "AIzaSyAPagXvglCXnK3neJwU50EiZnJPmdd__PM",
  // authDomain: "waypoooldemo.firebaseapp.com",
  // databaseURL: "https://waypoooldemo.firebaseio.com",
  // projectId: "waypoooldemo",
  // storageBucket: "waypoooldemo.appspot.com",
  // messagingSenderId: "1009109452629"



@NgModule({
  declarations: [
    MyApp
  

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    CommonModule,
    IonicStorageModule.forRoot()
  ],
 
  
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GoogleMaps,
   
    Firebase,
    Geolocation,
    EmailComposer,
    
    
    CallNumber,
    operationDBService

  ]
})
export class AppModule {}
