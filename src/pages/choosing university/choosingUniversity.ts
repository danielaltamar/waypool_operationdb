import { Component } from '@angular/core';
import { NavController, ViewController, IonicPage, NavParams } from 'ionic-angular';
import { operationDBService } from '../../services/operationDB.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Subject } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';


@IonicPage()

@Component({
  selector: 'page-choosingUniversity',
  templateUrl: 'choosingUniversity.html'
})
export class choosingUniversityPage {

 allUniversity:any =[];


 user:any;
 driver:any;
 unsubscribe = new Subject;
 showTimes:any;
 showReserves:any;
 showTrips:any;

  constructor(public navCtrl: NavController, public afDB: AngularFireDatabase,public navParams: NavParams ,public AngularFireAuth: AngularFireAuth,public viewCtrl: ViewController, public operationDBService:operationDBService) {    
      this.operationDBService.getUniversity()
        .subscribe( university => {      
          this.allUniversity = university;
          console.log(this.allUniversity);
      }) 
    
    }
chooseUniversity(university){
  this.navCtrl.push('operationDBPage',{university:university.name})
}
}
