webpackJsonp([3],{

/***/ 576:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "choosingUniversityPageModule", function() { return choosingUniversityPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__choosingUniversity__ = __webpack_require__(710);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var choosingUniversityPageModule = /** @class */ (function () {
    function choosingUniversityPageModule() {
    }
    choosingUniversityPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__choosingUniversity__["a" /* choosingUniversityPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__choosingUniversity__["a" /* choosingUniversityPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__choosingUniversity__["a" /* choosingUniversityPage */]
            ]
        })
    ], choosingUniversityPageModule);
    return choosingUniversityPageModule;
}());

//# sourceMappingURL=choosingUniversity.module.js.map

/***/ }),

/***/ 710:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return choosingUniversityPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_operationDB_service__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_database__ = __webpack_require__(187);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var choosingUniversityPage = /** @class */ (function () {
    function choosingUniversityPage(navCtrl, afDB, navParams, AngularFireAuth, viewCtrl, operationDBService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.afDB = afDB;
        this.navParams = navParams;
        this.AngularFireAuth = AngularFireAuth;
        this.viewCtrl = viewCtrl;
        this.operationDBService = operationDBService;
        this.allUniversity = [];
        this.unsubscribe = new __WEBPACK_IMPORTED_MODULE_4_rxjs__["Subject"];
        this.operationDBService.getUniversity()
            .subscribe(function (university) {
            _this.allUniversity = university;
            console.log(_this.allUniversity);
        });
    }
    choosingUniversityPage.prototype.chooseUniversity = function (university) {
        this.navCtrl.push('operationDBPage', { university: university.name });
    };
    choosingUniversityPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-choosingUniversity',template:/*ion-inline-start:"C:\Users\Daniel\Documents\waypool\test\waypool_operationDB\waypool_operationDB\src\pages\choosing university\choosingUniversity.html"*/'<ion-header class="bg-theme">\n\n    <ion-navbar hideBackButton="true">\n\n        <ion-title class="text-center">UNIVERSIDADES</ion-title>\n\n    </ion-navbar>\n\n    \n\n</ion-header>\n\n\n\n<ion-content class="bg-light">\n\n    <p class="important"> Escoja la universidad para ver sus estadísticas </p> \n\n    <div>      \n\n            <ion-card *ngFor="let university of allUniversity">                  \n\n                        <button class="btn bg-theme text-white rounded"  (click)="chooseUniversity(university)">{{university.name}}</button>                          \n\n            </ion-card>    \n\n               \n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Daniel\Documents\waypool\test\waypool_operationDB\waypool_operationDB\src\pages\choosing university\choosingUniversity.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__angular_fire_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["AngularFireAuth"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__services_operationDB_service__["a" /* operationDBService */]])
    ], choosingUniversityPage);
    return choosingUniversityPage;
}());

//# sourceMappingURL=choosingUniversity.js.map

/***/ })

});
//# sourceMappingURL=3.js.map