import { Component } from '@angular/core';
import { NavController, ViewController, IonicPage, NavParams, AlertController } from 'ionic-angular';
import { operationDBService } from '../../services/operationDB.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Subject } from 'rxjs';
import * as moment from 'moment';


@IonicPage()

@Component({
  selector: 'page-operationDB',
  templateUrl: 'operationDB.html'
})
export class operationDBPage {

 allUsers:any =[];
 drivers:any =[];
 allTrips:any =[];
 timesUserGoListride:any =[];
 allReserves:any =[];

 user:any;
 driver:any;
 unsubscribe = new Subject;
 showTimes:any;
 showReserves:any;
 showTrips:any;
 universityName:any;
 today:any;
 showDrivers:any;
 showUsers:any;
 allDrivers: any = [];
 driversIncompleteDocuments: any = [];
 showDriversIncomplete: any = [];
  constructor(public navCtrl: NavController, public afDB: AngularFireDatabase, public alertCtrl:AlertController,public AngularFireAuth: AngularFireAuth,public viewCtrl: ViewController,public navParams: NavParams ,public operationDBService:operationDBService) {    
   this.universityName = this.navParams.get('university');
    moment.locale('es'); //to make the date be in spanish  
   this.today = moment().format('MMMM Do YYYY, h:mm:ss a'); //set actual date


   console.log(this.universityName)
    this.operationDBService.getTripsInitiated(this.universityName)
      .subscribe( trips => {      
        this.allTrips = trips;
        console.log(this.allTrips);
        console.log(this.allTrips.length);
        this.showTrips = this.allTrips.length
    }) 
    this.operationDBService.getTheReserves(this.universityName)
    .subscribe( reserves => {      
      this.allReserves = reserves;    
      console.log(this.allReserves);
      console.log(this.allReserves.length);
      this.showReserves = this.allReserves.length
  }) 
  this.operationDBService.getTimesUserGoListride(this.universityName)
  .subscribe( times => {      
    this.timesUserGoListride = times;
    console.log(this.timesUserGoListride);
    console.log(this.timesUserGoListride.length);
    this.showTimes = this.timesUserGoListride.length
}) 
this.operationDBService.getTheUsers(this.universityName)
.subscribe( everything => {
  this.allUsers = everything 
    this.allUsers = everything 
  this.allUsers = everything 
  console.log(this.allUsers);
  // this.getDocumentsUsers()
})
 

    this.operationDBService.getTheDrivers(this.universityName)
    .subscribe( everything => {      
      this.drivers = everything;
      this.drivers.forEach(driver => {   
        this.driver = driver; 


   
    });
      this.getInfoDrivers()
      // console.log(this.drivers);
  }) 
  
}

  //unsubscribe before entering in the functionAAAAAAAAAAAAAAAAAAAAAAAA
  goToPanel(){
    this.navCtrl.push('ControlPanelPage',{university:this.universityName});
  }
  saveData(){
    this.operationDBService.sendData(this.today,this.showTrips,this.showReserves,this.showTimes);
    const alert = this.alertCtrl.create({
      title: 'GUARDADO EXITOSAMENTE',
      subTitle: 'Se acaban de guardar los datos exitosamente, FUCK YEAH!!!!!!!! ;D',
      buttons: ['EXCELENTISIMO']
    });
    alert.present();
  }
  //  daughter, 2 childs, children is plural, my son's name is daniel, ENGINEER, I WORK in Transelca, it is an energy company
//  getDocumentsUsers(){

//   this.allUsers.forEach(user => { 
//         this.user = user; 
//         // console.log("bien")
//         //--------------------------------------------
// //REMOVE PASSWORDS
//         // this.operationDBService.removePasswords(this.user.userId);


//         if(  user.documents === undefined ){
//           this.operationDBService.pushPhoneUsers(user.phone);
//         }else{    
//           console.log(' hay un driver en user ')
//       }  
//     })
//       // this.operationDBService.insertUser(user.userId,user);
    

//  }
 getInfoDrivers(){

    this.showDrivers=this.allDrivers.length
    this.showUsers = this.allUsers.length - this.showDrivers
    console.log(this.allDrivers);
    console.log(this.driversIncompleteDocuments);

  // ------------------------------------
  // GET DRIVERS'S PHONE
  this.drivers.forEach(driver => {   
    this.driver = driver; 

    if(this.driver.documents === undefined){
      
    }else{    
      // this.operationDBService.pushPhoneDrivers(driver.phone);
    }  

    if(this.driver.documents === undefined ){
    }else{    
      this.allDrivers.push(this.driver);
      if(this.driver.documents.id == false || this.driver.documents.license == false  ){
        // GET INCOMPLETE DRIVER DOCUMENTS
        this.driversIncompleteDocuments.push(this.driver);
        console.log('paso algo')
      }else{    
         
        
      }
      
  } 
  
        });
        this.showDriversIncomplete =this.driversIncompleteDocuments.length
        console.log(this.driversIncompleteDocuments);
        
 
 }
 unSubscribeServices(){
  this.unsubscribe.next();
  this.unsubscribe.complete();
}  
  dismiss() {
    }  
}
